import {Schema, model} from 'mongoose'
import bcrypt from 'bcryptjs'

const userSchema = new Schema({
  username: {
    type: String,
    unique: true
  },
  email: {
    type: String,
    unique: true
  },
  password: {
    type: String,
    unique: true
  },
  roles: [{
    ref: "Role",
    type: Schema.Types.ObjectId
  }]
},{
  timestamps: true,
  versionKey: false
});

userSchema.statics.encryptPassword = async (password) => {
  const salt = await bcrypt.genSalt(10);
  return await bcrypt.hash(password, salt);
};
userSchema.statics.comparePassword = async (password, resivereceivedPassworddPassword) => {
  return await bcrypt.compare(password, resivereceivedPassworddPassword)
};
userSchema.statics.testUser = async (param1, resivereceivedPassworddPassword) => {
  return await bcrypt.compare(param1, resivereceivedPassworddPassword);
  // return await param1+resivereceivedPassworddPassword;
}

export default model('User', userSchema);
