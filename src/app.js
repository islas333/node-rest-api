import express from 'express'
import morgan from 'morgan'
import pkg from '../package.json'

import {createRoles} from './libs/initialSetup'

import productsRoutes from './routes/products.routes'
import authRoute from './routes/auth.routes'
import testRoute from './routes/test.routes'

const app = express()
createRoles();

app.set('pkg', pkg)
app.use(express.json());
app.use(morgan('dev'));

app.get('/', (req, res) => {
  res.json({
    author: app.get('pkg').name,
    description: app.get('pkg').description,
    version: app.get('pkg').version,
  });
})

app.use('/api/products',productsRoutes)
app.use('/api/auth',authRoute)
app.use('/api/test',testRoute)

export default app;
