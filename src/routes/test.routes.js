import {Router} from 'express'
const router = Router()

import * as testCtrl from '../controllers/test.controller'

router.get('/', testCtrl.getTest)

export default router;