import Product from '../models/Product'

export const createProduct = async (req, res) => {
  // console.log(req.body);

  const {name, category, price, imgUrl} = req.body
  const newProduct = new Product({name, category, price, imgUrl});
  const productoSaved = await newProduct.save()

  // Metodo convesional
  // new Product({
  //   name: req.body.name, 
  //   category: req.body.category
  // })
  res.status(201).json(productoSaved)
}

export const getProduct = async (req, res) => {
  const products = await Product.find();
  res.status(200).json(products)
}

export const getProductById = async (req, res) => {
  const products = await Product.findById(req.params.productId)
  res.status(200).json(products);
}

export const updateProductById = async (req, res) => {
  // console.log(req.params.productId)
  // console.log(req.body.name)
  const products = await Product.findByIdAndUpdate(req.params.productId, req.body, {
    new: true
  })
  res.status(200).json(products)
}

export const deleteProductById = async (req, res) => {
  const product = await Product.findByIdAndDelete(req.params.productId)
  res.status(204).json()
}