//Crear el archivo package para las configuraciones
npm init -y

// Librerias necesarias
npm i express bcryptjs cors dotenv jsonwebtoken mongoose morgan helmet

//Compilador de ES7 ++
npm i @babel/core @babel/cli @babel/node @babel/preset-env nodemon -D